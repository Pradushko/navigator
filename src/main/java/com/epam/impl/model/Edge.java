package com.epam.impl.model;

public class Edge {

    /**
     * point forming edge with vertex
     */
    private final Vertex target;

    /**
     * edge length
     */
    private final int distance;

    /**
     *  the cost of travel along the edge
     */
    private final int cost;

    public Edge(Vertex target, int distance, int cost) {
        this.target = target;
        this.distance = distance;
        this.cost = cost;
    }

    public Vertex getTarget() {
        return target;
    }

    public int getDistance() {
        return distance;
    }

    public int getCost() {
        return cost;
    }
}
