package com.epam.impl.model;

import java.util.ArrayList;
import java.util.List;

public class Vertex implements Comparable<Vertex> {
    private final String name;

    /**
     * this list stores all edges forming with this vertex
     */
    private List<Edge> edges = new ArrayList<>();

    /**
     * distance traveled through this point
     * default maximum number
     */
    private int minDistance = Integer.MAX_VALUE;

    /**
     * the cost of travel through this point
     */
    private int cost;

    /**
     * previous visited point
     */
    private Vertex previous;

    public Vertex(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public int getMinDistance() {
        return minDistance;
    }

    public int getCost() {
        return cost;
    }

    public Vertex getPrevious() {
        return previous;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public void setMinDistance(int minDistance) {
        this.minDistance = minDistance;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setPrevious(Vertex previous) {
        this.previous = previous;
    }

    public int compareTo(Vertex other) {
        return Integer.compare(minDistance, other.minDistance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vertex vertex = (Vertex) o;

        return name.equals(vertex.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }
}