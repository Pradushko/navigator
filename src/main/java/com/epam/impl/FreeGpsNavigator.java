package com.epam.impl;

import com.epam.api.GpsNavigator;
import com.epam.api.Path;
import com.epam.impl.model.Edge;
import com.epam.impl.model.Vertex;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class FreeGpsNavigator implements GpsNavigator {
    private static Logger log = Logger.getLogger(FreeGpsNavigator.class);

    /**
     * contains all the vertices
     */
    private Set<Vertex> vertices = new HashSet<>();

    @Override
    public void readData(String filePath) {
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            lines.forEach(
                    s -> {
                        String[] line = s.replaceAll("\\s+$", "").split(" ");

                        String startPoint = line[0];
                        String endPoint = line[1];
                        int distance = Integer.parseInt(line[2]);
                        int cost = Integer.parseInt(line[3]);

                        /**
                         * check if there is such a vertex
                         * if there is no vertex, we create and add to the collection
                         */
                        Vertex startVertex = getVertex(startPoint);
                        Vertex endVertex = getVertex(endPoint);

                        if (startVertex == null) {
                            startVertex = new Vertex(startPoint);
                            vertices.add(startVertex);
                        }
                        if (endVertex == null) {
                            endVertex = new Vertex(endPoint);
                            vertices.add(endVertex);
                        }

                        /**
                         * the second point of the segment is an edge with the first point
                         * create an edge object
                         * add it to the list of edges of the first point
                         */
                        Edge edge = new Edge(endVertex, distance, cost);
                        startVertex.getEdges().add(edge);
                    });
        } catch (IOException e) {
            System.out.println("File not found " + e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        } catch (UncheckedIOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Path findPath(String pointA, String pointB) {
        try {
            /**
             * we get the initial and final vertex by name
             * from the collection of vertices
             */
            Vertex vertexA = getVertex(pointA);
            Vertex vertexB = getVertex(pointB);

            vertexA.setMinDistance(0);
            vertexA.setPrevious(new Vertex("point"));

            PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
            vertexQueue.add(vertexA);

            /**
             * Dijkstra's algorithm is implemented in this cycle.
             * The shortest path is not calculated for all points, as in the classic version,
             * but only to the end point of the route.
             * If there is no route to the end point, then we get a NullPointerException.
             * If the point is absent in the source data, we will get a NullPointerException.
             */


//            the cycle works until it reaches point B
            while (vertexQueue.peek() != vertexB) {
                Vertex u = vertexQueue.poll();

                // Visit each edge exiting u
                for (Edge e : u.getEdges()) {
                    Vertex v = e.getTarget();
                    int distance = e.getDistance();

                    int distanceThroughU = u.getMinDistance() + distance;

                    if (distanceThroughU < v.getMinDistance()) {
                        vertexQueue.remove(v);
                        v.setMinDistance(distanceThroughU);
                        v.setPrevious(u);
                        v.setCost(e.getCost());
                        vertexQueue.add(v);
                    }
                }
            }

            /**
             *  we found the shortest distance from point A to point B
             */
            log.info("shortest distance " + vertexB.getMinDistance());

            /**
             * list for storing the shortest path vertices
             */
            List<String> shortestPath = new ArrayList<>();
            int cost = 0;

            /**
             * get the shortest route
             * go from point B in the opposite direction
             */
            for (Vertex vertex = vertexB; vertex.getPrevious() != null; vertex = vertex.getPrevious()) {
                shortestPath.add(vertex.getName());
                // calculate the cost
                cost += vertex.getCost() * (vertex.getMinDistance() - vertex.getPrevious().getMinDistance());
            }

            Collections.reverse(shortestPath);
            Path path = new Path(shortestPath, cost);
            log.info(path);

            return path;
        } catch (NullPointerException e) {
            System.out.println("Point does not exist or is not reachable");
            return null;
        }
    }

    /**
     * @param name - vertex name
     * @return a vertex object by name or null
     */
    public Vertex getVertex(String name) {
        Vertex vertex = vertices.stream().filter(v -> v.getName().equals(name)).findFirst().orElse(null);
        return vertex;
    }
}

