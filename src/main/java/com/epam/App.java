package com.epam;

import com.epam.api.GpsNavigator;
import com.epam.api.Path;
import com.epam.impl.FreeGpsNavigator;

/**
 * This class app demonstrates how your implementation of {@link com.epam.api.GpsNavigator} is intended to be used.
 */
public class App {
    public static void main(String[] args) {
        final GpsNavigator navigator = new FreeGpsNavigator();
        navigator.readData("D:\\Gps\\road_map.ext");

        final Path path = navigator.findPath("A", "F");
        System.out.println(path);
    }
}
