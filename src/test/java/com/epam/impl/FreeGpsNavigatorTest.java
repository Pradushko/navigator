package com.epam.impl;

import com.epam.impl.model.Edge;
import com.epam.impl.model.Vertex;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

public class FreeGpsNavigatorTest extends Assert {
    private Set<Vertex> vertices = new HashSet<>();

    @Before
    public void readData() throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("test.txt")))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] lines = line.replaceAll("\\s+$", "").split(" ");

                String startPoint = lines[0];
                String endPoint = lines[1];
                int distance = Integer.parseInt(lines[2]);
                int cost = Integer.parseInt(lines[3]);

                Vertex startVertex = getVertex(startPoint);
                Vertex endVertex = getVertex(endPoint);

                if (startVertex == null) {
                    startVertex = new Vertex(startPoint);
                    vertices.add(startVertex);
                }
                if (endVertex == null) {
                    endVertex = new Vertex(endPoint);
                    vertices.add(endVertex);
                }
                Edge edge = new Edge(endVertex, distance, cost);
                startVertex.getEdges().add(edge);
            }
        }
    }

    public Vertex getVertex(String name) {
        Vertex vertex = vertices.stream().filter(v -> v.getName().equals(name)).findFirst().orElse(null);
        return vertex;
    }

    @Test
    public void findPath() {
        String pointA = "F";
        String pointB = "B";
        Vertex vertexA = getVertex(pointA);
        Vertex vertexB = getVertex(pointB);

        vertexA.setMinDistance(0);
        vertexA.setPrevious(new Vertex("point"));

        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
        vertexQueue.add(vertexA);

        while (vertexQueue.peek() != vertexB) {
            Vertex u = vertexQueue.poll();
            for (Edge e : u.getEdges()) {
                Vertex v = e.getTarget();
                int distance = e.getDistance();
                int distanceThroughU = u.getMinDistance() + distance;
                if (distanceThroughU < v.getMinDistance()) {
                    vertexQueue.remove(v);
                    v.setMinDistance(distanceThroughU);
                    v.setPrevious(u);
                    v.setCost(e.getCost());
                    vertexQueue.add(v);
                }
            }
        }
        StringBuilder shortestPath = new StringBuilder();
        int cost = 0;

        for (Vertex vertex = vertexB; vertex.getPrevious() != null; vertex = vertex.getPrevious()) {
            cost += vertex.getCost() * (vertex.getMinDistance() - vertex.getPrevious().getMinDistance());
            shortestPath = shortestPath.append(vertex.getName());
        }
        assertEquals(16, cost);
        assertEquals("BACEF", shortestPath.toString());
    }
}